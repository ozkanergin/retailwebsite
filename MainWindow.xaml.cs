﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RetailWebsite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , INotifyPropertyChanged
    {
        #region INOTIFYPROPERTY
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyRaised(string propertyname)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
            }
        }
        #endregion

        private List<Card_Type> card_type_list;
        public List<Card_Type> Card_Type_List
        {
            get { return card_type_list; }
            set {
                card_type_list = value;
                OnPropertyRaised(nameof(Card_Type_List));

            }
        }

        private Card_Type card_type_property;
        public Card_Type Card_Type_Property
        {
            get { return card_type_property; }
            set { card_type_property = value; }
        }


        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            card_type_list = Enum.GetValues(typeof(Card_Type)).Cast<Card_Type>().ToList();
        }
        /*****************************************************************************************************************************/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (false == double.TryParse(textbox_price.Text.ToString(), out double Price)) return;
            Category Category = true == checkbox_phone.IsChecked ? Category.PHONE : Category.OTHER;
            Find_Last_Price(Card_Type_Property, (bool)radiobutton_affiliate.IsChecked, (bool)radiobutton_over_two_years.IsChecked, Category, Price);
        }
        /*****************************************************************************************************************************/
        public double Find_Last_Price(Card_Type Card_Type, bool IsAffiliate, bool IsOverTwoYears, Category Category, double Price)
        {
            User user       = new User(Card_Type, IsAffiliate, IsOverTwoYears);
            Product product = new Product(Category, Price);

            StateMachine sm = new StateMachine(user, product);

            sm.Run_Machine();

            label_price.Content = product.Price.ToString();

            return product.Price;
        }
    }
}
