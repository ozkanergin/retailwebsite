﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public class Product
    {
        public Category Category = Category.PHONE;
        public double Price;

        public Product(Category Category, double Price)
        {
            this.Category = Category;
            this.Price    = Price;
        }
    }
}
