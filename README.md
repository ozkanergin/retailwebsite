In the this code there are five basic classes which are called StateMachine, StateTransition, ConditionChecker and User-Product
Firstly all informations are taken. 
These are user's informations such as Card Type, Affiliate situation and over 2 years for customer period. 
Category and price information for product.
User and product class keeps these informations.

In order to calculate the last price a state machine code has been written.
Transition variable is Hashset. Therefore it keeps only unique StateTransition elements.
Equals and HashCode functions are overriden to do this.
If there is a special condition, transition list is specialized. 
As you can see on the example code, if category of product is phone, different transition list is created.
Every element of transition list is StateTransition variable which has state, next state and function fields in it.
StateMachine starts from idle state. Every state has function to decide go to next state or output.
Functions are written in the ConditionChecker class.
If state is output or command is break, machine is going to stop. 

For different scenarios, same function might not be last option or does not check any condition.
Therefore current command or current state has to be checked to stop the state machine.  
