﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RetailWebsite
{
    public class StateMachine
    {
        #region VARIABLES
        HashSet<StateTransition> transitions;
        public System_State Current_State   { get; private set; } = System_State.IDLE;
        public Command      Current_Command { get; private set; } = Command.NEXT_STATE;
        #endregion VARIABLES
        /******************************************************************************************************************************************/
        public StateMachine(User user, Product product)
        {
            ConditionChecker checker = new ConditionChecker(user, product);

            switch (product.Category)
            {
                case Category.PHONE:
                    {

                        transitions = new HashSet<StateTransition>
                        {
                              new StateTransition(){ State = System_State.IDLE        , Next_State = System_State.DOLLAR_200  , function = checker.Idle       },
                              new StateTransition(){ State = System_State.DOLLAR_200  , Next_State = System_State.OUTPUT_STATE, function = checker.Dollar_200 },
                        };
                    }
                    break;
                case Category.OTHER:
                    {
                        transitions = new HashSet<StateTransition>
                        {
                            new StateTransition(){ State = System_State.IDLE        , Next_State = System_State.PERC_30     , function = checker.Idle                  },
                            new StateTransition(){ State = System_State.PERC_30     , Next_State = System_State.PERC_20     , function = checker.UserHasGoldCard       }, 
                            new StateTransition(){ State = System_State.PERC_20     , Next_State = System_State.PERC_10     , function = checker.UserHasSilverCard     }, 
                            new StateTransition(){ State = System_State.PERC_10     , Next_State = System_State.OVER_2_YEARS, function = checker.UserIsAffiliate       },
                            new StateTransition(){ State = System_State.OVER_2_YEARS, Next_State = System_State.DOLLAR_200  , function = checker.UserHasOverTwoYears   },
                            new StateTransition(){ State = System_State.DOLLAR_200  , Next_State = System_State.OUTPUT_STATE, function = checker.Dollar_200            }, 
                        };
                    }
                    break;
                default:
                    {
                        MessageBox.Show("Category has not set.", "Error", MessageBoxButton.OK,MessageBoxImage.Error);
                    }
                    break;
            }
        }
        /******************************************************************************************************************************************/
        public System_State Run_Machine()
        {

            if(Current_Command == Command.BREAK || Current_State == System_State.OUTPUT_STATE)
            {
                return System_State.OUTPUT_STATE;
            }
            else
            {
                Current_Command = transitions.Single(x => x.State == Current_State).Update();
                Current_State   = transitions.Single(x => x.State == Current_State).Next_State;
                return Run_Machine();
            }
        }
    }
}
