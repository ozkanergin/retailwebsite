﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public class StateTransition : IState
    {
        public Func<Command> function;
        private System_State state;
        private System_State next_state;


        public Func<Command> Function   { get => function; set => function = value; }
        public System_State  State      { get => state; set => state = value; }
        public System_State  Next_State { get => next_state; set => next_state = value; }

        public Command Update()
        {
            return function.Invoke();
        }

        public override bool Equals(object obj)     // It is overriden. Instance of this class is used for hashset collection.
        {
            var State_Object = obj as StateTransition;

            return State_Object.state      == state && 
                   State_Object.next_state == next_state && 
                   State_Object.function   == function;   
        }

        public override int GetHashCode()
        {
            return state.GetHashCode();
        }
    }
}
